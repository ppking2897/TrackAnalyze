using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication2.Models;

namespace UnitTestProject3
{

    // 00:不同地點同司機
    // 01:同地點重複上傳
    // 02:第一個為下車
    // 03:同地點上下車
    // 04:同地點不同司機

    //過濾流程: 過濾條碼 --> 同地點不同司機(04) --> 不同地點同司機(00) --> 第一個為下車(02) --> 同地點重複上傳(01) --> 同地點上下車(03) 

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void 同地點重複狀態測試()
        {
            //重複上車資料測試
            IScanRecordAnalyzer<IScanRecordWithShip> scan = new ScanRecordAnalyzerForShip(SamePlaceSameHandling());

            scan.Analyze();

            IEnumerable<IScanRecordWithShip> IgnoreRecords = scan.IgnoreRecords;
            IEnumerable<IScanRecordWithShip> NormalRecords = scan.NormalRecords;
            IDictionary<int, IEnumerable<IScanRecordWithShip>> ConfusingRecords = scan.ConfusingRecords;


            //重複資料五筆 Result只剩下一筆  AnalyzeResult為分析過後有疑慮的資料 Result為分析過後沒問題的資料
            Assert.IsTrue(IgnoreRecords.Count() == 0);
            Assert.IsTrue(NormalRecords.Count()== 1);
            Assert.IsTrue(ConfusingRecords.Count() != 0);

        }

        [TestMethod]
        public void 同地點正常上下車過濾測試()
        {
            //同個地點上下車測試  

            IScanRecordAnalyzer<IScanRecordWithShip> scan = new ScanRecordAnalyzerForShip(SamePlaceNormalHandling());

            scan.Analyze();

            IEnumerable<IScanRecordWithShip> IgnoreRecords = scan.IgnoreRecords;
            IEnumerable<IScanRecordWithShip> NormalRecords = scan.NormalRecords;
            IDictionary<int, IEnumerable<IScanRecordWithShip>> ConfusingRecords = scan.ConfusingRecords;

            //同地點上下車4次 第五次為上車 Result只剩下一筆  AnalyzeResult為分析過後有疑慮的資料 Result為分析過後沒問題的資料
            Assert.IsTrue(IgnoreRecords.Count() == 0);
            Assert.IsTrue(NormalRecords.Count() == 1);
            Assert.IsTrue(ConfusingRecords.Count() != 0);
        }

        [TestMethod]
        public void 同地點不同司機測試()
        {
            //同地點不同司機上傳  
            IScanRecordAnalyzer<IScanRecordWithShip> scan = new ScanRecordAnalyzerForShip(SamePlaceDifferentDriver());

            scan.Analyze();

            IEnumerable<IScanRecordWithShip> IgnoreRecords = scan.IgnoreRecords;
            IEnumerable<IScanRecordWithShip> NormalRecords = scan.NormalRecords;
            IDictionary<int, IEnumerable<IScanRecordWithShip>> ConfusingRecords = scan.ConfusingRecords;

            Assert.IsTrue(IgnoreRecords.Count() == 0);
            Assert.IsTrue(NormalRecords.Count() == 0);
            Assert.IsTrue(ConfusingRecords.Count() != 0);
        }

        [TestMethod]
        public void 第一筆都為下車狀態測試()
        {
            //第一個為下車紀錄  
            IScanRecordAnalyzer<IScanRecordWithShip> scan = new ScanRecordAnalyzerForShip(EveHandling());

            scan.Analyze();

            IEnumerable<IScanRecordWithShip> IgnoreRecords = scan.IgnoreRecords;
            IEnumerable<IScanRecordWithShip> NormalRecords = scan.NormalRecords;
            IDictionary<int, IEnumerable<IScanRecordWithShip>> ConfusingRecords = scan.ConfusingRecords;



            //同地點下上下上 下 因為第一次都為下  AnalyzeResult為分析過後有疑慮的資料 Result為分析過後沒問題的資料
            Assert.IsTrue(IgnoreRecords.Count() == 0);
            Assert.IsTrue(NormalRecords.Count() == 1);
            Assert.IsTrue(ConfusingRecords.Count() != 0);
        }

        [TestMethod]
        public void 過濾不同條碼測試()
        {
            //不同條碼 過濾出比較多的條碼去分析
            IScanRecordAnalyzer<IScanRecordWithShip> scan = new ScanRecordAnalyzerForShip(DifferentSheetNumber());

            scan.Analyze();

            IEnumerable<IScanRecordWithShip> IgnoreRecords = scan.IgnoreRecords;
            IEnumerable<IScanRecordWithShip> NormalRecords = scan.NormalRecords;
            IDictionary<int, IEnumerable<IScanRecordWithShip>> ConfusingRecords = scan.ConfusingRecords;

            //AnalyzeResult 所出來的是不同條碼的結果
            Assert.IsTrue(IgnoreRecords.Count() > 0);
            Assert.IsTrue(NormalRecords.Count() == 1);
            Assert.IsTrue(ConfusingRecords.Count() != 0);
        }

        [TestMethod]
        public void 不同地點同司機()
        {
            //不同條碼 過濾出比較多的條碼去分析
            IScanRecordAnalyzer<IScanRecordWithShip> scan = new ScanRecordAnalyzerForShip(DifferentPlaceSameDriver());

            scan.Analyze();

            IEnumerable<IScanRecordWithShip> IgnoreRecords = scan.IgnoreRecords;
            IEnumerable<IScanRecordWithShip> NormalRecords = scan.NormalRecords;
            IDictionary<int, IEnumerable<IScanRecordWithShip>> ConfusingRecords = scan.ConfusingRecords;

            //AnalyzeResult 所出來的是不同條碼的結果
            Assert.IsTrue(IgnoreRecords.Count() == 0);
            Assert.IsTrue(NormalRecords.Count() > 0);
            Assert.IsTrue(ConfusingRecords.Count() != 0);
        }

        [TestMethod]
        public void 分析的資料為NULL()
        {
            //不同條碼 過濾出比較多的條碼去分析
            IScanRecordAnalyzer<IScanRecordWithShip> scan = new ScanRecordAnalyzerForShip(null);

            scan.Analyze();

            IEnumerable<IScanRecordWithShip> IgnoreRecords = scan.IgnoreRecords;
            IEnumerable<IScanRecordWithShip> NormalRecords = scan.NormalRecords;
            IDictionary<int, IEnumerable<IScanRecordWithShip>> ConfusingRecords = scan.ConfusingRecords;

            //AnalyzeResult 所出來的是不同條碼的結果
            Assert.IsTrue(IgnoreRecords == null);
            Assert.IsTrue(NormalRecords == null );
            Assert.IsTrue(ConfusingRecords == null);
        }

        public IEnumerable<ScanRecord> EveHandling()
        {
            ScanRecord scan01 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 10, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan02 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 09, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan03 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 11, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan04 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 08, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan05 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 08, 30, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan06 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 13, 30, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan07 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 14, 30, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            List<ScanRecord> records = new List<ScanRecord>
            {
                scan01,
                scan02,
                scan03,
                scan04,
                scan05,
                scan06,
                scan07
            };
            IEnumerable<ScanRecord> scanRecords = records;
            return scanRecords;
        }

        public IEnumerable<ScanRecord> SamePlaceSameHandling()
        {
            ScanRecord scan01 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 10, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan02 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 09, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan03 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 11, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan04 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 08, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan05 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 08, 30, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            List<ScanRecord> records = new List<ScanRecord>
            {
                scan01,
                scan02,
                scan03,
                scan04,
                scan05
            };
            IEnumerable<ScanRecord> scanRecords = records;
            return scanRecords;
        }

        public IEnumerable<ScanRecord> SamePlaceNormalHandling()
        {
            ScanRecord scan01 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 10, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan02 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 09, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan03 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 11, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan04 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 08, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan05 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 13, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 123456,
                ShipKind = ScanShipKind.Board
            };


            List<ScanRecord> records = new List<ScanRecord>
            {
                scan01,
                scan02,
                scan03,
                scan04,
                scan05
            };
            IEnumerable<ScanRecord> scanRecords = records;
            return scanRecords;
        }

        public IEnumerable<ScanRecord> DifferentPlaceSameDriver()
        {
            ScanRecord scan01 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 10, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 333333,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan02 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 09, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 222222,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan03 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 11, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 444444,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan04 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 08, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 888888,
                StationSN = 111111,
                ShipKind = ScanShipKind.Board
            };


            List<ScanRecord> records = new List<ScanRecord>
            {
                scan01,
                scan02,
                scan03,
                scan04
            };
            IEnumerable<ScanRecord> scanRecords = records;
            return scanRecords;
        }

        public IEnumerable<ScanRecord> SamePlaceDifferentDriver()
        {
            ScanRecord scan01 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 10, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 333333,
                StationSN = 141414,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan02 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 09, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 222222,
                StationSN = 141414,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan03 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 11, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 444444,
                StationSN = 141414,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan04 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 08, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 111111,
                StationSN = 141414,
                ShipKind = ScanShipKind.Board
            };


            List<ScanRecord> records = new List<ScanRecord>
            {
                scan01,
                scan02,
                scan03,
                scan04
            };
            IEnumerable<ScanRecord> scanRecords = records;
            return scanRecords;
        }

        //public IEnumerable<ScanRecord> DifferentPlaceDifferentDriver()
        //{
        //    ScanRecord scan01 = new ScanRecord
        //    {
        //        ScanTime = new DateTime(2018, 08, 20, 10, 00, 00),
        //        Latitude = 24.123,
        //        Longitude = 120.111,
        //        SheetNumber = "123444",
        //        TruckerSN = 333333,
        //        StationSN = 333333,
        //        Handing = 02
        //    };

        //    ScanRecord scan02 = new ScanRecord
        //    {
        //        ScanTime = new DateTime(2018, 08, 20, 09, 00, 00),
        //        Latitude = 24.123,
        //        Longitude = 120.111,
        //        SheetNumber = "123444",
        //        TruckerSN = 222222,
        //        StationSN = 222222,
        //        Handing = 01
        //    };

        //    ScanRecord scan03 = new ScanRecord
        //    {
        //        ScanTime = new DateTime(2018, 08, 20, 11, 00, 00),
        //        Latitude = 24.123,
        //        Longitude = 120.111,
        //        SheetNumber = "123444",
        //        TruckerSN = 444444,
        //        StationSN = 444444,
        //        Handing = 02
        //    };

        //    ScanRecord scan04 = new ScanRecord
        //    {
        //        ScanTime = new DateTime(2018, 08, 20, 08, 00, 00),
        //        Latitude = 24.123,
        //        Longitude = 120.111,
        //        SheetNumber = "123444",
        //        TruckerSN = 111111,
        //        StationSN = 111111,
        //        Handing = 01
        //    };


        //    List<ScanRecord> records = new List<ScanRecord>
        //    {
        //        scan01,
        //        scan02,
        //        scan03,
        //        scan04
        //    };
        //    IEnumerable<ScanRecord> scanRecords = records;
        //    return scanRecords;
        //}

        public IEnumerable<ScanRecord> DifferentSheetNumber()
        {
            ScanRecord scan01 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 10, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "444555",
                TruckerSN = 333333,
                StationSN = 333333,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan02 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 09, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "111111",
                TruckerSN = 222222,
                StationSN = 222222,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan03 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 11, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 444444,
                StationSN = 444444,
                ShipKind = ScanShipKind.Unboard
            };

            ScanRecord scan04 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 08, 00, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 111111,
                StationSN = 111111,
                ShipKind = ScanShipKind.Board
            };

            ScanRecord scan05 = new ScanRecord
            {
                ScanTime = new DateTime(2018, 08, 20, 08, 10, 00),
                Latitude = 24.123,
                Longitude = 120.111,
                SheetNumber = "123444",
                TruckerSN = 111111,
                StationSN = 111111,
                ShipKind = ScanShipKind.Board
            };


            List<ScanRecord> records = new List<ScanRecord>
            {
                scan01,
                scan02,
                scan03,
                scan04,
                scan05
            };
            IEnumerable<ScanRecord> scanRecords = records;
            return scanRecords;
        }

    }


}
