﻿using System;
using System.Collections.Generic;
using System.Linq;

public interface IScanRecordAnalyzer<T>
{
    void Analyze();
    /// <summary>
    /// 忽略的記錄
    /// </summary>
    IEnumerable<T> IgnoreRecords { get; }
    /// <summary>
    /// 正常的記錄
    /// </summary>
    IEnumerable<T> NormalRecords { get; }
    /// <summary>
    /// 異議狀態的記錄
    /// </summary>
    IDictionary<int, IEnumerable<T>> ConfusingRecords { get; }
}

/// <summary>
/// 載運裝卸類型
/// </summary>
public enum ScanShipKind : int
{
    /// <summary>
    /// 上車
    /// </summary>
    Board = 1,
    /// <summary>
    /// 下車
    /// </summary>
    Unboard = 2,
}

public interface IScanRecord
{
    Guid ID { get; }
    /// <summary>
    /// 聯單號碼(條碼)
    /// </summary>
    string SheetNumber { get; }
    /// <summary>
    /// 掃描時間
    /// </summary>
    DateTime ScanTime { get; }
}

public interface IScanRecordWithShip : IScanRecord
{
    /// <summary>
    /// 司機關聯編號
    /// </summary>
    int TruckerSN { get; }
    /// <summary>
    /// 站點關聯編號
    /// </summary>
    int StationSN { get; }
    /// <summary>
    /// 掃描地點緯度
    /// </summary>
    double Latitude { get; }
    /// <summary>
    /// 抓描地點經度
    /// </summary>
    double Longitude { get; }
    /// <summary>
    /// 載運裝卸類型
    /// </summary>
    ScanShipKind ShipKind { get; }
}

public interface IScanRecordWithApply : IScanRecord
{
    /// <summary>
    /// 聯單關聯編號
    /// </summary>
    int ApplySN { get; }
    /// <summary>
    /// 託運日期
    /// </summary>
    DateTime ConveyDate { get; }
    /// <summary>
    /// 是否已關聯聯單
    /// </summary>
    /// <returns></returns>
    bool IsCorrelative();
}

public class ScanRecord : IScanRecord, IScanRecordWithApply, IScanRecordWithShip
{
    public Guid ID { set; get; }

    public string SheetNumber { set; get; }

    public DateTime ScanTime { set; get; }

    public int ApplySN { set; get; }

    public DateTime ConveyDate { set; get; }

    public int TruckerSN { set; get; }

    public int StationSN { set; get; }

    public double Latitude { set; get; }

    public double Longitude { set; get; }

    public ScanShipKind ShipKind { set; get; }

    public bool IsCorrelative()
    {
        throw new NotImplementedException();
    }
}

public class ScanRecordAnalyzerForApply : IScanRecordAnalyzer<IScanRecordWithApply>
{
    public IEnumerable<IScanRecordWithApply> IgnoreRecords => throw new NotImplementedException();

    public IEnumerable<IScanRecordWithApply> NormalRecords => throw new NotImplementedException();

    public IDictionary<int, IEnumerable<IScanRecordWithApply>> ConfusingRecords => throw new NotImplementedException();

    public void Analyze()
    {
        throw new NotImplementedException();
    }

    public enum ConfusingState
    {

    }
}

public class ScanRecordAnalyzerForShip : IScanRecordAnalyzer<IScanRecordWithShip>
{
    private IEnumerable<IScanRecordWithShip> records;
    private List<IScanRecordWithShip> TruckerScanRecord = new List<IScanRecordWithShip>();
    private Dictionary<int, IEnumerable<IScanRecordWithShip>> AnalyzeScanRecord = new Dictionary<int, IEnumerable<IScanRecordWithShip>>();

    public IEnumerable<IScanRecordWithShip> IgnoreRecords { set; get; }

    public IEnumerable<IScanRecordWithShip> NormalRecords { set; get; }

    public IDictionary<int, IEnumerable<IScanRecordWithShip>> ConfusingRecords { set; get; }

    public ScanRecordAnalyzerForShip(IEnumerable<IScanRecordWithShip> records)
    {
        this.records = records;
    }

    public void Analyze()
    {
        
        if (records == null)
        {
            IgnoreRecords = null;
            NormalRecords = null;
            ConfusingRecords = null;
            return;
        }

        //找出所有條碼,不重複
        List<string> SameSheetNumber = new List<string>();
        for (int i = 0; i < records.Count(); i++)
        {
            SameSheetNumber.Add(records.ElementAt(i).SheetNumber);
        }

        IEnumerable<string> clearList = SameSheetNumber.Distinct();

        TruckerScanRecord = records.OrderBy(x => x.ScanTime).ToList();

        //條碼過濾 TruckerScanRecord 裡面做處理,再給其他方法使用該物件
        DifferentSheetNumber(clearList);

        IEnumerable<IScanRecordWithShip> scans01 = SamePlaceDifferentDriver();
        if (scans01.Count() > 0)
        {
            AnalyzeScanRecord.Add( (int)ConfusingState.SamePlaca_DifferentDriver, scans01);
        }

        IEnumerable<IScanRecordWithShip> scans02 = DifferentPlaceSameDriver();
        if (scans02.Count() > 0)
        {
            AnalyzeScanRecord.Add((int)ConfusingState.DifferentPlace_SameDriver, scans02);
        }

        IEnumerable<IScanRecordWithShip> scans03 = FirstUnboardAnalyze();
        if (scans03.Count() > 0)
        {
            AnalyzeScanRecord.Add((int)ConfusingState.FirstUnboard, scans03);
        }

        IEnumerable<IScanRecordWithShip> scans04 = SamePlaceRepeatUpload();
        if (scans04.Count() > 0)
        {
            AnalyzeScanRecord.Add((int)ConfusingState.SamePlace_RepeatUpload, scans04);
        }

        IEnumerable<IScanRecordWithShip> scans05 = SamePlaceNormalHandling();
        if (scans05.Count() > 0)
        {
            AnalyzeScanRecord.Add((int)ConfusingState.SamePlace_NormalSatus, scans05);
        }

        NormalRecords = TruckerScanRecord;

        ConfusingRecords = AnalyzeScanRecord;

    }

    public enum ConfusingState :int
    {
        // 00:不同地點同司機
        // 01:同地點重複上傳
        // 02:第一個為下車
        // 03:同地點上下車
        // 04:同地點不同司機
        
        DifferentPlace_SameDriver = 00,

        SamePlace_RepeatUpload=01,

        FirstUnboard = 02,

        SamePlace_NormalSatus = 03,

        SamePlaca_DifferentDriver = 04

    }


    IEnumerable<IScanRecordWithShip> FirstUnboardAnalyze()
    {
        //第一次為下車時丟出異常

        List<IScanRecordWithShip> items = new List<IScanRecordWithShip>();
        for (int i = 0; i < TruckerScanRecord.Count(); i += 2)
        {
            //2個紀錄為一組判斷第一個是否為下車
            if (TruckerScanRecord.ElementAt(i).ShipKind == ScanShipKind.Unboard)
            {
                //若為5次紀錄為 下上下上 下  為了避免最後一個下出錯分開判斷
                if ((i + 1) < TruckerScanRecord.Count())
                {
                    items.Add(TruckerScanRecord.ElementAt(i));
                    items.Add(TruckerScanRecord.ElementAt(i + 1));
                    TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                    TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                    i = -2;
                }
                else
                {
                    items.Add(TruckerScanRecord.ElementAt(i));
                    TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                }
            }
        }
        return items;
    }

    IEnumerable<IScanRecordWithShip> DifferentPlaceSameDriver()
    {
        //相同司機 不同地點  相同狀態 

        List<IScanRecordWithShip> items = new List<IScanRecordWithShip>();
        for (int i = 0; i < TruckerScanRecord.Count(); i++)
        {
            if ((i + 1) >= TruckerScanRecord.Count()) return items;

            if (TruckerScanRecord.ElementAt(i).StationSN == 0)
            {
                double lat01 = TruckerScanRecord.ElementAt(i).Latitude;
                double lng01 = TruckerScanRecord.ElementAt(i).Longitude;
                double lat02 = TruckerScanRecord.ElementAt(i + 1).Latitude;
                double lng02 = TruckerScanRecord.ElementAt(i + 1).Longitude;

                if (calcDistance(lat01, lng01, lat02, lng02) <= 50)
                {
                    if (TruckerScanRecord.ElementAt(i).TruckerSN == TruckerScanRecord.ElementAt(i + 1).TruckerSN)
                    {
                        if (TruckerScanRecord.ElementAt(i).ShipKind == TruckerScanRecord.ElementAt(i + 1).ShipKind)
                        {
                            items.Add(TruckerScanRecord.ElementAt(i));
                            TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                            i = -1;
                        }
                    }
                       
                }
            }

            else if (TruckerScanRecord.ElementAt(i).StationSN != TruckerScanRecord.ElementAt(i + 1).StationSN)
            {
                if (TruckerScanRecord.ElementAt(i).TruckerSN == TruckerScanRecord.ElementAt(i + 1).TruckerSN)
                {
                    if (TruckerScanRecord.ElementAt(i).ShipKind == TruckerScanRecord.ElementAt(i + 1).ShipKind)
                    {
                        items.Add(TruckerScanRecord.ElementAt(i));
                        TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                        i = -1;
                    }
                }
            }
        }
        return items;
    }

    IEnumerable<IScanRecordWithShip> SamePlaceNormalHandling()
    {
        List<IScanRecordWithShip> samePlaceItem = new List<IScanRecordWithShip>();
        for (int i = 0; i < TruckerScanRecord.Count(); i++)
        {
            //地點相同 司機相同 狀態相同 

            for (int j = i + 1; j < TruckerScanRecord.Count(); j++)
            {

                if (TruckerScanRecord.ElementAt(i).StationSN == 0)
                {
                    double lat01 = TruckerScanRecord.ElementAt(i).Latitude;
                    double lng01 = TruckerScanRecord.ElementAt(i).Longitude;
                    double lat02 = TruckerScanRecord.ElementAt(i + 1).Latitude;
                    double lng02 = TruckerScanRecord.ElementAt(i + 1).Longitude;

                    if (calcDistance(lat01, lng01, lat02, lng02) <= 50)
                    {
                        //同個地點上車又下車
                        if (TruckerScanRecord.ElementAt(i).ShipKind == ScanShipKind.Board& TruckerScanRecord.ElementAt(j).ShipKind == ScanShipKind.Unboard)
                        {
                            samePlaceItem.Add(TruckerScanRecord.ElementAt(i));
                            samePlaceItem.Add(TruckerScanRecord.ElementAt(j));
                            TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(j));
                            TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                            i = -1;
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else if (TruckerScanRecord.ElementAt(i).StationSN == TruckerScanRecord.ElementAt(j).StationSN)
                {

                    if (TruckerScanRecord.ElementAt(i).TruckerSN == TruckerScanRecord.ElementAt(j).TruckerSN)
                    {
                        //同個地點上車又下車
                        if (TruckerScanRecord.ElementAt(i).ShipKind == ScanShipKind.Board & TruckerScanRecord.ElementAt(j).ShipKind == ScanShipKind.Unboard)
                        {
                            samePlaceItem.Add(TruckerScanRecord.ElementAt(i));
                            samePlaceItem.Add(TruckerScanRecord.ElementAt(j));
                            TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(j));
                            TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                            i = -1;
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }
        return samePlaceItem;
    }

    IEnumerable<IScanRecordWithShip> SamePlaceRepeatUpload()
    {
        List<IScanRecordWithShip> sameHandling = new List<IScanRecordWithShip>();
        for (int i = 0; i < TruckerScanRecord.Count(); i++)
        {
            //地點相同 司機相同 狀態相同異常
            if ((i + 1) >= TruckerScanRecord.Count()) return sameHandling;
            if (TruckerScanRecord.ElementAt(i).StationSN == 0)
            {
                double lat01 = TruckerScanRecord.ElementAt(i).Latitude;
                double lng01 = TruckerScanRecord.ElementAt(i).Longitude;
                double lat02 = TruckerScanRecord.ElementAt(i + 1).Latitude;
                double lng02 = TruckerScanRecord.ElementAt(i + 1).Longitude;

                if (calcDistance(lat01, lng01, lat02, lng02) <= 50)
                {
                    if (TruckerScanRecord.ElementAt(i).ShipKind== TruckerScanRecord.ElementAt(i + 1).ShipKind)
                    {
                        sameHandling.Add(TruckerScanRecord.ElementAt(i + 1));
                        TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i + 1));
                        i = -1;
                    }
                }
            }
            else if (TruckerScanRecord.ElementAt(i).StationSN == TruckerScanRecord.ElementAt(i + 1).StationSN)
            {
                if (TruckerScanRecord.ElementAt(i).TruckerSN == TruckerScanRecord.ElementAt(i + 1).TruckerSN)
                {

                    if (TruckerScanRecord.ElementAt(i).ShipKind == TruckerScanRecord.ElementAt(i + 1).ShipKind)
                    {
                        //TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(j));
                        //sameHandling.Add(TruckerScanRecord.ElementAt(i));
                        sameHandling.Add(TruckerScanRecord.ElementAt(i + 1));
                        TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i + 1));
                        i = -1;
                    }
                }
            }

        }
        return sameHandling;
    }

    IEnumerable<IScanRecordWithShip> SamePlaceDifferentDriver()
    {
        List<IScanRecordWithShip> items = new List<IScanRecordWithShip>();
        for (int i = 0; i < TruckerScanRecord.Count(); i += 2)
        {
            //地點相同 司機不同 

            for (int j = i + 1; j < TruckerScanRecord.Count(); j++)
            {
                if (TruckerScanRecord.ElementAt(i).StationSN == 0)
                {
                    double lat01 = TruckerScanRecord.ElementAt(i).Latitude;
                    double lng01 = TruckerScanRecord.ElementAt(i).Longitude;
                    double lat02 = TruckerScanRecord.ElementAt(i + 1).Latitude;
                    double lng02 = TruckerScanRecord.ElementAt(i + 1).Longitude;

                    if (calcDistance(lat01, lng01, lat02, lng02) <= 50)
                    {
                        if (TruckerScanRecord.ElementAt(i).TruckerSN != TruckerScanRecord.ElementAt(j).TruckerSN)
                        {
                            items.Add(TruckerScanRecord.ElementAt(i));
                            items.Add(TruckerScanRecord.ElementAt(j));
                            TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                            TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                            i = -1;
                            break;
                        }
                    }
                }

                else if (TruckerScanRecord.ElementAt(i).StationSN == TruckerScanRecord.ElementAt(j).StationSN)
                {
                    if (TruckerScanRecord.ElementAt(i).TruckerSN != TruckerScanRecord.ElementAt(j).TruckerSN)
                    {
                        items.Add(TruckerScanRecord.ElementAt(i));
                        items.Add(TruckerScanRecord.ElementAt(j));
                        TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                        TruckerScanRecord.Remove(TruckerScanRecord.ElementAt(i));
                        i = -2;
                        break;
                    }
                }
            }
        }
        return items;

    }

    IEnumerable<IScanRecordWithShip> DifferentPlaceDifferentDriver()
    {
        List<IScanRecordWithShip> items = new List<IScanRecordWithShip>();
        for (int i = 0; i < TruckerScanRecord.Count(); i++)
        {
            //地點不同 司機不同 

            for (int j = i + 1; j < TruckerScanRecord.Count(); j++)
            {
                if (TruckerScanRecord.ElementAt(i).StationSN == 0)
                {
                    double lat01 = TruckerScanRecord.ElementAt(i).Latitude;
                    double lng01 = TruckerScanRecord.ElementAt(i).Longitude;
                    double lat02 = TruckerScanRecord.ElementAt(i + 1).Latitude;
                    double lng02 = TruckerScanRecord.ElementAt(i + 1).Longitude;

                    if (calcDistance(lat01, lng01, lat02, lng02) <= 50)
                    {
                        if (TruckerScanRecord.ElementAt(i).TruckerSN != TruckerScanRecord.ElementAt(j).TruckerSN)
                        {
                            items.Add(TruckerScanRecord.ElementAt(i));
                            items.Add(TruckerScanRecord.ElementAt(j));
                            break;
                        }
                    }
                }
                else if (TruckerScanRecord.ElementAt(i).StationSN != TruckerScanRecord.ElementAt(j).StationSN)
                {
                    if (TruckerScanRecord.ElementAt(i).TruckerSN != TruckerScanRecord.ElementAt(j).TruckerSN)
                    {
                        items.Add(TruckerScanRecord.ElementAt(i));
                        items.Add(TruckerScanRecord.ElementAt(j));
                        break;
                    }
                }
            }
        }
        return items;
    }

    void DifferentSheetNumber(IEnumerable<string> clearList)
    {
        List<IScanRecordWithShip> items = new List<IScanRecordWithShip>();
        List<IScanRecordWithShip> anotherSheetNumber = new List<IScanRecordWithShip>();
        int count = 0;
        for (int i = 0; i < clearList.Count(); i++)
        {

            //var find = from data in TruckerScanRecord
            //           where data.SheetNumber == clearList.ElementAt(i) orderby data.ScanTime select data;

            //List<ScanRecord> scan = new List<ScanRecord>();

            //foreach (var data in find)
            //{
            //    scan.Add(data);
            //}

            List<IScanRecordWithShip> list = TruckerScanRecord.FindAll(x => x.SheetNumber == clearList.ElementAt(i));

            if (list.Count() > count)
            {
                if (items.Count() != 0)
                {
                    anotherSheetNumber.AddRange(items);
                }

                items = list;
                count = list.Count();
            }
            else
            {
                anotherSheetNumber.AddRange(list);
            }
        }
      
        IgnoreRecords = anotherSheetNumber;
        TruckerScanRecord = items;
    }

    readonly double EARTH_RADIUS = 6378.137; // 地球半径

    static double rad(double d)
    {
        return d * Math.PI / 180.0; // 计算弧长
    }

    double calcDistance(double Lat1, double Lng1, double Lat2,
                           double Lng2)
    {

        double radLat1 = rad(Lat1);
        double radLat2 = rad(Lat2);
        double a = radLat1 - radLat2;
        double b = rad(Lng1) - rad(Lng2);
        double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2)
                + Math.Cos(radLat1) * Math.Cos(radLat2)
                * Math.Pow(Math.Sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        s = s * 1000; // 换算成米
        return s;

    }
}